import axios from 'axios';
import semver from 'semver';

function findPeerVersions(versionsArray: string[], inputVersion: string): string[] {
  const stuff = versionsArray.filter(version => semver.gte(version, inputVersion));
  if (stuff.length === 0 || stuff.length === 1) {
    return stuff;
  }

  return stuff.filter(version => semver.diff(version, inputVersion) === 'patch');
}

function findClosestVersion(versionsArray: string[], version: string): [string, string[]] {
  let minVersion = version;
  let compatibleVersions = findPeerVersions(versionsArray, minVersion);
  for (const minorVersion of [ ...Array(semver.minor(minVersion)).keys() ].reverse()) {
    if (compatibleVersions.length === 0) {
      const nextLower = `${semver.major(minVersion)}.${minorVersion}.0`;
      const stuff = findPeerVersions(versionsArray, nextLower);
      if (stuff.length > 0) {
        return [ minVersion, [ minVersion ] ];
      }
      minVersion = nextLower;
      compatibleVersions = stuff;
    } else {
      return [ minVersion, compatibleVersions ];
    }
  }

  return [ `${semver.major(version)}.0.0`, [ `${semver.major(version)}.0.0` ] ];
}

// eslint-disable-next-line import/no-default-export
export default async function getVersion(packageName: string, version: string): Promise<string> {
  if (semver.clean(version) === null) {
    throw new Error(`Invalid version: ${version}`);
  }

  const packageData = await axios.get(`https://registry.yarnpkg.com/${packageName}/`);
  const versions = Object.keys(packageData.data.versions);

  const [ minVersion, compatibleVersions ] = findClosestVersion(versions, `${semver.major(version)}.${semver.minor(version)}.0`);
  const latestVersion = compatibleVersions[compatibleVersions.length - 1];

  const versionDiff = semver.diff(latestVersion, minVersion);

  if (versionDiff === 'patch') {
    return semver.inc(latestVersion, 'patch');
  }

  // make sure it's not already published
  if (versions.some(someVersion => someVersion === latestVersion)) {
    return semver.inc(latestVersion, 'patch');
  }

  return minVersion;
}
