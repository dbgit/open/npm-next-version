/* eslint-disable import/order,no-unused-vars,@typescript-eslint/no-unused-vars,import/first */
import findNextVersion from '../src/index';

// actual releases of moment
const versionsTest: string[] = [
  '1.0.0',
  '1.0.1',
  '1.1.0',
  '1.1.1',
  '1.2.0',
  '1.3.0',
  '1.4.0',
  '1.5.0',
  '1.5.1',
  '1.6.0',
  '1.6.1',
  '1.6.2',
  '1.7.0',
  '1.7.1',
  '1.7.2',
  '2.0.0',
  '2.1.0',
  '2.2.1',
  '2.3.0',
  '2.3.1',
  '2.4.0',
  '2.5.0',
  '2.5.1',
  '2.6.0',
  '2.7.0',
  '2.8.1',
  '2.8.2',
  '2.8.3',
  '2.8.4',
  '2.9.0',
  '2.10.2',
  '2.10.3',
  '2.10.5',
  '2.10.6',
  '2.11.0',
  '2.11.1',
  '2.11.2',
  '2.12.0',
  '2.13.0',
  '2.14.0',
  '2.14.1',
  '2.15.0',
  '2.15.1',
  '2.15.2',
  '2.16.0',
  '2.17.0',
  '2.17.1',
  '2.18.0',
  '2.18.1',
  '2.19.0',
  '2.19.1',
  '2.19.2',
  '2.19.3',
  '2.19.4',
  '2.20.0',
  '2.20.1',
  '2.21.0',
  '2.22.0',
  '2.22.1',
  '2.22.2',
  '2.23.0',
  '2.24.0'
];
jest.mock('axios', () => ({
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  get: () => ({ data: { versions: versionsTest.reduce<{[key: string]: null}>((result, val) => {
    result[val] = null;

    return result;
  }, {}) } })
}));
import axios from 'axios';

describe('Find the correct version', () => {
  const tests: TestData[] = [
    {
      version: '3.0.0',
      expected: '3.0.0'
    },
    {
      version: '3.5.15',
      expected: '3.0.0'
    },
    {
      version: '2.17.0',
      expected: '2.17.2'
    },
    {
      version: '2.28.18',
      expected: '2.25.0'
    },
    {
      version: '2.22.4444',
      expected: '2.22.3'
    }
    // ToDo: figure this out
    // {
    //   version: '2.4.0',
    //   expected: '2.4.1'
    // }
  ];

  tests.forEach(({ version, expected }) => {
    test(`expect ${version} to be ${expected}`, async () => {
      const result = await findNextVersion('lodash', version);
      expect(result).toBe(expected);
    });
  });
});

interface TestData {
  version: string;
  expected: string;
}
