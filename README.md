# NPM-next-version

NPM-next-version tries to guess the next best version for a given package based on what is currently published on NPM.

## Examples

Lets say your package name is `foobar` and your latest deployed version is `2.4.6`

```js
const NpmNextVersion = require('npm-next-version');
const { name, version } = require('./package');

(async () => {
  const nextVersion = await NpmNextVersion(
    name,   // foobar
    version // 2.4.0
  );
  console.log(nextVersion); // 2.4.7
})();
```

It automatically increased the to the next patch version.

**But i would like to upgrade a major/minor version?**

Just increase the version in you package.json to `2.5.0` or `3.0.0`.

NPM-next-version will also make sure you don't accidentally skip a version:
e.g. if you increase the version in you package.json to `2.6.0` it will return `2.5.0` instead.

The package will not change your package.json, it will just suggest the next version. What you do with that suggestion is up to you.
